import { Injectable } from '@angular/core';
import { getFirstTemplatePass } from '@angular/core/src/render3/state';
import { HttpClient } from '@angular/common/http'
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }


  login(username: string, password: string) {

      //"email": "ww", "password":"pw"
      return this.http.post<any>('http://localhost:8080/RESTfulDEMO/users/login', { "username": username, "password": password })
      .toPromise()
      .then(
        res => { 
          if(res && res.token){
          console.log(JSON.stringify(res.token));
          localStorage.setItem('currentUser', res.token);
          return true;// Success
          }
          console.log("login not successful: " + JSON.stringify(res.token));
          return false;
          
        },
        error => {
          console.log("an error occurred" + error);
          return false;
          
        }
      );
     
    }

  logout() {
    localStorage.removeItem('currentUser');
  }

  isLoggedIn() {
    if (localStorage.getItem('currentUser')) {
      return true;
    }
    return false;
  }
}
