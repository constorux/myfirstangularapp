import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';
import { trigger, state, style, animate, transition, } from '@angular/animations';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [trigger('openClose', [
    // ...
    state('open', style({
      height: '25%',
    })),
    state('closed', style({
      height: '15%',
    })),
    transition('open => closed', [
      animate('0.3s')
    ]),
    transition('closed => open', [
      animate('0.4s')
    ]),
  ]),
  ],
})
export class LoginComponent implements OnInit {

  isOpen: boolean = false;
  submitted: boolean = false;
  loginError: boolean = false;
  returnUrl: string;
  loginForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router, private activatedRoute: ActivatedRoute, ) {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      currentpassword: ['', Validators.required]

    })
  }

  ngOnInit() {
    
    this.authService.logout();

    if (!this.isOpen) {
      setTimeout(() => this.isOpen = true)
    }

    // get return url from route parameters or default to '/'
    this.returnUrl = this.activatedRoute.snapshot.queryParams['returnUrl'] || '/account';
  }

  onSubmit() {

    if (this.isOpen) {
      setTimeout(() => this.isOpen = false)
    }
    this.loginError = false;
    //this.isOpen = true;
    this.submitted = true;

    if (this.loginForm.invalid) {
      setTimeout(() => this.isOpen = true)
      return;
    }

    this.authService.login(this.loginForm.controls.username.value, this.loginForm.controls.password.value).then(result => {
      if (result) {
        this.router.navigate([this.returnUrl]);
        console.log(this.returnUrl)
      } else {
        this.loginError = true;
        if (!this.isOpen) {
          setTimeout(() => this.isOpen = true)
        }
        //this.loginForm.reset();
      }
    });

  }
}
