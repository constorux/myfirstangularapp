import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  submitted : boolean = false;
  messageForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.messageForm = this.formBuilder.group({ 
      message: ['', Validators.required],
      name: ['', Validators.required] 
      
    })
  }

  onSubmit(){
    this.submitted = true;

    if(this.messageForm.invalid){
      return;
    }

    alert("name: "  + this.messageForm.controls.name.value + "\r\n" + 
    "message: "  + this.messageForm.controls.message.value)
  }

  ngOnInit() {
  }

}
